<?php

namespace Laracasts\Contracts;

use Illuminate\Support\Collection;
use Laracasts\Lesson;
use Laracasts\Series;

interface UserLearning
{
    /**
     * Mark a particular lesson as completed by the user
     *
     * @param Lesson $lesson
     */
    public function completeLesson(Lesson $lesson);

    /**
     * How much of series did the user complete,
     * in percentage
     *
     * @param Series $series
     * @return float|int
     */
    public function percentageCompletedForSeries(Series $series);

    /**
     * Get number of completed lessons for a series
     *
     * @param Series $series
     * @return int
     */
    public function getNumberOfCompletedLessonsForASeries(Series $series);

    /**
     * Get all lessons a user completed in a series
     *
     * @param Series $series
     * @return array
     */
    public function getCompletedLessonsForASeries(Series $series);

    /**
     * Check if the user has started a series
     *
     * @param Series $series
     * @return bool
     */
    public function hasStartedSeries(Series $series);

    /**
     * Get a collection of completed lessons from a series
     *
     * @param Series $series
     * @return Collection
     */
    public function getCompletedLessons(Series $series);
}
