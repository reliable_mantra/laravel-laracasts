<?php

namespace Laracasts\Traits;

use Illuminate\Support\Collection;
use Laracasts\Lesson;
use Laracasts\Series;
use Redis;

/**
 * Trait Learning
 * @package Laracasts\Traits
 * @property $id
 */
trait Learning
{
    /**
     * Mark a particular lesson as completed by the user
     *
     * @param Lesson $lesson
     */
    public function completeLesson(Lesson $lesson)
    {
        Redis::sadd("user:{$this->id}:series:{$lesson->series->id}", $lesson->id);
    }

    /**
     * How much of series did the user complete,
     * in percentage
     *
     * @param Series $series
     * @return float|int
     */
    public function percentageCompletedForSeries(Series $series)
    {
        $numberOfLessonsInSeries = $series->lessons->count();
        $numberOfCompletedLessons = $this->getNumberOfCompletedLessonsForASeries($series);

        return ($numberOfCompletedLessons / $numberOfLessonsInSeries) * 100;
    }

    /**
     * Get number of completed lessons for a series
     *
     * @param Series $series
     * @return int
     */
    public function getNumberOfCompletedLessonsForASeries(Series $series)
    {
        return count($this->getCompletedLessonsForASeries($series));
    }

    /**
     * Get all lessons a user completed in a series
     *
     * @param Series $series
     * @return array
     */
    public function getCompletedLessonsForASeries(Series $series)
    {
        return Redis::smembers("user:{$this->id}:series:{$series->id}");
    }

    /**
     * Check if the user has started a series
     *
     * @param Series $series
     * @return bool
     */
    public function hasStartedSeries(Series $series)
    {
        return $this->getNumberOfCompletedLessonsForASeries($series) > 0;
    }

    /**
     * Get a collection of completed lessons from a series
     *
     * @param Series $series
     * @return Collection
     */
    public function getCompletedLessons(Series $series)
    {
        return Lesson::whereIn('id', $this->getCompletedLessonsForASeries($series))->get();
    }

    /**
     * Check if a user has completed a lesson
     *
     * @param Lesson $lesson
     * @return bool
     */
    public function hasCompletedLesson(Lesson $lesson)
    {
        return in_array(
            $lesson->id,
            $this->getCompletedLessonsForASeries($lesson->series)
        );
    }

    /**
     * Get series being watched
     *
     * @return Collection
     */
    public function seriesBeingWatched()
    {
        $keys = Redis::keys("user:{$this->id}:series:*");

        return collect($keys)->map(function ($key) {
            $seriesId = explode(':', $key)[3];
            return Series::find($seriesId);
        })->filter();
    }

    /**
     * Get total number of completed lessons
     *
     * @return int
     */
    public function getTotalNumberOfCompletedLessons()
    {
        $keys = Redis::keys("user:{$this->id}:series:*");
        $result = 0;

        collect($keys)->map(function ($key) use (&$result) {
            $result = $result + count(Redis::smembers($key));
        });

        return $result;
    }

    /**
     * Get next lesson to watch
     *
     * @param Series $series
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Lesson|Lesson[]|null
     */
    public function getNextLessonToWatch(Series $series)
    {
        $completedLessons = $this->getCompletedLessonsForASeries($series);

        return Lesson::find(end($completedLessons))->getNextLesson();
    }
}
