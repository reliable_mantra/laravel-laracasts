<?php

namespace Laracasts\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\User;

class ProfilesController extends Controller
{
    /**
     * Show user profile
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(User $user)
    {
        return view('templates.profile')->with([
            'user' => $user,
            'series' => $user->seriesBeingWatched(),
        ]);
    }

    /**
     * Update user details
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect("/profile/{$user->id}")->with('success', 'Profile details updated successfully.');
    }

    /**
     * Update card
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function cardUpdate(Request $request)
    {
        $this->validate($request, [
            'stripeToken' => 'required',
        ]);

        $user = auth()->user();

        $user->updateCard($request->stripeToken);
        return response()->json('ok');
    }
}
