<?php

namespace Laracasts\Http\Controllers;

use Laracasts\Lesson;
use Laracasts\Series;

class WatchSeriesController extends Controller
{
    /**
     * Show series
     *
     * @param Series $series
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Series $series)
    {
        $user = auth()->user();

        return redirect()->route('series.watch.lesson', [
            'series' => $series->slug,
            'lesson' => ($user->hasStartedSeries($series)) ? $user->getNextLessonToWatch($series) : $series->lessons->first()->id,
        ]);
    }

    /**
     * Show a lesson
     *
     * @param Series $series
     * @param Lesson $lesson
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showLesson(Series $series, Lesson $lesson)
    {
        if ($lesson->premium && !auth()->user()->subscribed()) {
            return redirect('/subscribe');
        }

        return view('templates.watch', [
            'series' => $series,
            'lesson' => $lesson,
        ]);
    }

    /**
     * Complete a lesson
     *
     * @param Lesson $lesson
     * @return \Illuminate\Http\JsonResponse
     */
    public function completeLesson(Lesson $lesson)
    {
        auth()->user()->completeLesson($lesson);
        return response()->json([
            'status' => 'ok',
        ]);
    }
}
