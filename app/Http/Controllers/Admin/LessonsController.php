<?php

namespace Laracasts\Http\Controllers\Admin;

use Laracasts\Http\Controllers\Controller;
use Laracasts\Http\Requests\Lessons\LessonCreateRequest;
use Laracasts\Http\Requests\Lessons\LessonUpdateRequest;
use Laracasts\Lesson;
use Laracasts\Series;

class LessonsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Series $series
     * @param LessonCreateRequest $request
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(Series $series, LessonCreateRequest $request)
    {
        return $series->lessons()->create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Series $series
     * @param Lesson $lesson
     * @param LessonUpdateRequest $request
     * @return Lesson
     */
    public function update(Series $series, Lesson $lesson, LessonUpdateRequest $request)
    {
        $lesson->update($request->all());

        return $lesson->fresh();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Series $series
     * @param Lesson $lesson
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Series $series, Lesson $lesson)
    {
        $lesson->delete();

        return response()->json(['status' => 'ok'], 200);
    }
}
