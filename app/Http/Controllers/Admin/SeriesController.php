<?php

namespace Laracasts\Http\Controllers\Admin;

use Laracasts\Http\Controllers\Controller;
use Laracasts\Http\Requests\Series\SeriesCreateRequest;
use Laracasts\Http\Requests\Series\SeriesUpdateRequest;
use Laracasts\Series;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.series.index')
            ->with('series', Series::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.series.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SeriesCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SeriesCreateRequest $request)
    {
        return $request->uploadSeriesImage()
                       ->storeSeries();
    }

    /**
     * Display the specified resource.
     *
     * @param Series $series
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Series $series)
    {
        return view('admin.series.show')
            ->with('series', $series);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Series  $series
     * @return \Illuminate\Http\Response
     */
    public function edit(Series $series)
    {
        return view('admin.series.edit')
            ->with('series', $series);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SeriesUpdateRequest $request
     * @param Series $series
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SeriesUpdateRequest $request, Series $series)
    {
        return $request->updateSeries($series);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Series $series
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Series $series)
    {
        $series->delete();

        return response()->json(['status' => 'ok'], 200);
    }
}
