<?php

namespace Laracasts\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\User;

class ConfirmEmailController extends Controller
{
    /**
     * Confirm user email endpoint
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        $user = User::where('confirm_token', request('token'))->first();

        if (!$user) {
            session()->flash('error', 'Confirmation token not recognized.');
            return redirect('/');
        }

        $user->confirm();
        session()->flash('success', 'Your email has been confirmed.');
        return redirect('/');
    }
}
