<?php

namespace Laracasts\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Series;

class FrontendController extends Controller
{
    /**
     * Homepage
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function welcome()
    {
        return view('welcome')
            ->with('series', Series::all());
    }

    /**
     * Show all series
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAllSeries()
    {
        return view('templates.series-all')
            ->with(['series' => Series::all()]);
    }

    /**
     * Show particular series
     *
     * @param Series $series
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function series(Series $series)
    {
        return view('templates.series')
            ->with(['series' => $series]);
    }
}
