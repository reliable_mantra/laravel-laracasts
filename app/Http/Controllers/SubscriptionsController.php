<?php

namespace Laracasts\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\User;

class SubscriptionsController extends Controller
{
    /**
     * Show subscription form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showSubscriptionForm()
    {
        if (auth()->user()->subscribed()) {
            return redirect('/profile/' . auth()->user()->id);
        }

        return view('templates.subscribe');
    }

    /**
     * Subscribe
     *
     * @param Request $request
     * @return \Laravel\Cashier\Subscription
     */
    public function subscribe(Request $request)
    {
        return auth()->user()->newSubscription(User::SUBSCRIPTION, $request->plan)
                     ->create($request->stripeToken);
    }

    /**
     * Change subscription plan
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function change(Request $request)
    {
        $this->validate($request, [
            'plan' => 'required',
        ]);

        $user = auth()->user();
        $userPlan = $user->subscriptions->first()->stripe_plan;

        if ($request->plan !== $userPlan) {
            $user->subscription(User::SUBSCRIPTION)->swap($request->plan);
        }

        return redirect()->back()->with('success', 'Subscription plan updated successfully.');
    }
}
