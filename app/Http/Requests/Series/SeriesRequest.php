<?php

namespace Laracasts\Http\Requests\Series;

use Illuminate\Foundation\Http\FormRequest;

class SeriesRequest extends FormRequest
{
    public $fileName;

    /**
     * Upload series image
     *
     * @return $this
     */
    public function uploadSeriesImage()
    {
        $uploadedImage = $this->image;

        $this->fileName = slugify($this->title) . '.' . $uploadedImage->getClientOriginalExtension();

        $uploadedImage->storePubliclyAs('public/series', $this->fileName);

        return $this;
    }
}
