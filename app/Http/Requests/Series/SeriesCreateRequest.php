<?php

namespace Laracasts\Http\Requests\Series;

use Laracasts\Http\Requests\Series\SeriesRequest;
use Laracasts\Series;

class SeriesCreateRequest extends SeriesRequest
{
    public $fileName;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'image' => ['required', 'image'],
        ];
    }

    /**
     * Store series
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeSeries()
    {
        $series = Series::create([
            'user_id' => auth()->user()->id,
            'title' => $this->title,
            'description' => $this->description,
            'image_url' => 'series/' . $this->fileName,
            'featured' => ($this->featured) ? true : false,
        ]);

        session()->flash('success', 'Series created successfully');

        return redirect()->route('series.show', $series->slug);
    }
}
