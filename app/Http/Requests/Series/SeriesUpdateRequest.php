<?php

namespace Laracasts\Http\Requests\Series;

use Laracasts\Http\Requests\Series\SeriesRequest;
use Laracasts\Series;

class SeriesUpdateRequest extends SeriesRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
        ];
    }

    /**
     * Update series
     *
     * @param Series $series
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeries(Series $series)
    {
        if ($this->hasFile('image')) {
            $series->image_url = 'series/' . $this->uploadSeriesImage()->fileName;
        }

        $series->title = $this->title;
        $series->description = $this->description;
        $series->featured = ($this->featured) ? true : false;

        $series->save();

        session()->flash('success', 'Successfully updated series');

        return redirect()->route('series.index');
    }
}
