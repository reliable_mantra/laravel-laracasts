<?php

namespace Laracasts\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class GlobalViewParams
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        View::share('laravel', [
            'csrfToken' => csrf_token(),
            'baseUrl' => url('/'),
            'routes' => collect(Route::getRoutes()->getRoutesByName())->map(function ($route) {
                return $route->uri;
            })->toArray(),
            'authUser' => auth()->user(),
            'env' => collect([
                'STRIPE_KEY' => config('services.stripe.key'),
            ]),
        ]);

        return $next($request);
    }
}
