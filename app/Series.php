<?php

namespace Laracasts;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    protected $guarded = ['slug'];

    protected $with = ['lessons'];

    /**
     * Autofill slug
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->slug = slugify($model->title);
        });

        self::updating(function ($model) {
            $model->slug = slugify($model->title);
        });

        self::saving(function ($model) {
            $model->slug = slugify($model->title);
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Lesson belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Series has many lessons
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    /**
     * Return the public path for series image
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return asset('storage' . $this->image_url);
    }

    /**
     * Get ascending ordered lessons by episode number
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getOrderedLessons()
    {
        return $this->lessons()->orderBy('episode_number', 'asc')->get();
    }
}
