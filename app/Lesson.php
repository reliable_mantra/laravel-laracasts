<?php

namespace Laracasts;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $guarded = [];

    /**
     * Lesson belongs to a series
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function series()
    {
        return $this->belongsTo(Series::class);
    }

    /**
     * Get next lesson
     *
     * @return mixed
     */
    public function getNextLesson()
    {
        $nextLesson = $this->series->lessons()->where('episode_number', '>', $this->episode_number)
                    ->orderBy('episode_number', 'asc')
                    ->first();

        if ($nextLesson) {
            if (!auth()->user() && $nextLesson->premium) {
                return $this;
            }

            return $nextLesson;
        }

        return $this;
    }

    /**
     * Get previous lesson
     *
     * @return mixed
     */
    public function getPrevLesson()
    {
        $prevLesson = $this->series->lessons()->where('episode_number', '<', $this->episode_number)
                    ->orderBy('episode_number', 'desc')
                    ->first();

        if ($prevLesson) {
            if (!auth()->user() && $prevLesson->premium) {
                return $this;
            }

            return $prevLesson;
        }

        return $this;
    }
}
