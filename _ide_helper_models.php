<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace Laracasts{
/**
 * Laracasts\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $confirm_token
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereConfirmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\User whereUsername($value)
 */
	class User extends \Eloquent {}
}

namespace Laracasts{
/**
 * Laracasts\Series
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $image_url
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $image_path
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laracasts\Lesson[] $lessons
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series whereImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Series whereUpdatedAt($value)
 */
	class Series extends \Eloquent {}
}

namespace Laracasts{
/**
 * Laracasts\Lesson
 *
 * @property int $id
 * @property int $series_id
 * @property string $title
 * @property string $description
 * @property int $episode_number
 * @property string $video_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Laracasts\Series $series
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereEpisodeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereSeriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Laracasts\Lesson whereVideoId($value)
 */
	class Lesson extends \Eloquent {}
}

