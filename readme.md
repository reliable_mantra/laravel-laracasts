## About

* This is a SAAS project similar to laracasts.com where you have courses and lessons from which some are free and some are paid.
Fot the ones that are not free we have a subscription based on stripe payment system.
* TDD (Test Driven Development) approach is used so there are unit tests for all key features.
* Frontend is created with Vue.js
* The project uses this themeforest template: https://themeforest.net/item/thesaas-responsive-bootstrap-saas-software-webapp-template/19778599 

## Single responsibility traits

Laravel uses a concept of single responsibility traits and in this way the classes are separated into several traits, each being responsible for a particular functionality.  
In order to rewrite the default behaviour you can just override trait methods inside the class that uses them.

## App Name

You can change the app name using **php artisan app:name Laracasts**.  
This will change the default *App* namespace to *Laracasts*.

## Eager Loading

Eager loading is useful for reducing number of queries being made.
You can set it on a model level (load every time a model is being fetched) by passing the relationships to be eager loaded to the protected **$with** parameter:

```php
<?php

protected $with = ['lessons'];

?>
```

Or you can eager load during the fetching process, depending on your use case.

## Setters and Getters (Mutators and Accessors)

To define an **accessor**, create a **getFooAttribute** method on your model where *Foo* is the "studly" cased name of the column you wish to access.

```php
<?php

/**
 * Get the user's first_name.
 *
 * @param  string  $value
 * @return string
 */
public function getFirstNameAttribute($value)
{
    return ucfirst($value);
}

?>
```

To define a **mutator**, define a **setFooAttribute** method on your model where *Foo* is the "studly" cased name of the column you wish to set.

```php
<?php

/**
 * Set the user's first_name.
 *
 * @param  string  $value
 * @return void
 */
public function setFirstNameAttribute($value)
{
    $this->attributes['first_name'] = strtolower($value);
}

?>
```

## Custom Routes File

*RouteServiceProvider* has a **map** method where you need to add your mapping:

```php
<?php

/**
 * Define the admin routes for the application
 *
 * @return void
 */
protected function mapAdminRoutes()
{
    Route::prefix('admin')
         ->middleware('admin')
         ->namespace($this->namespace . '\Admin')
         ->group(base_path('routes/admin.php'));
}

?>
```

The **namespace** method used above sets controllers for this group into a separate folder called *Admin*.

You can also create a separate middleware group for the new route group you created by adding it in the *App/Http/Kernel.php* protected **$middlewareGroups** parameter:

```php
<?php

/**
 * The application's route middleware groups.
 *
 * @var array
 */
protected $middlewareGroups = [
    'web' => [
        \Laracasts\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        // \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Laracasts\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Laracasts\Http\Middleware\GlobalViewParams::class,
    ],

    'api' => [
        'throttle:60,1',
        'bindings',
    ],

    'admin' => [
        'web',
        \Laracasts\Http\Middleware\Administrator::class,
    ],
];

?>
```

## Route model binding

Route model binding is a Laravel functionality that enables you to automatically bind a request id to the appropriate model.  
This can also be customized so that some other request parameter gets bound to a particular model. The way you do this is by overriding *Illuminate\Database\Eloquent\Model* method called **getRouteKeyName**:

```php
<?php

/**
 * Get the route key for the model.
 *
 * @return string
 */
public function getRouteKeyName()
{
    return 'slug';
}

?>
```

Also, there's explicit model binding witch can be set up in the *RouteServiceProvider* by adding this to the **boot** method:

```php
<?php

Route::model('series', Series::class);

?>
```

Next, define a route that contains a {series} parameter:

```php
<?php

Route::get('/series/{series}', 'FrontendController@series')->name('series');

?>
```

If you wish to use your own resolution logic, you may use the Route::bind method:

```php
<?php

Route::bind('series_by_id', function ($value) {
    return Series::findOrFail($value);
});

?>
```

Next, define a route that contains a {series_by_id} parameter:

```php
<?php

Route::resource('{series_by_id}/lessons', 'LessonsController')->except(['index', 'create', 'show', 'edit']);

?>
```

## Global View Params

This is solved using a middleware called GlobalViewParams.php witch is set only for the "web" middleware group.

```php
<?php

public function handle($request, Closure $next)
{
    View::share('laravel', [
        'csrfToken' => csrf_token(),
        'baseUrl' => url('/'),
        'routes' => collect(Route::getRoutes()->getRoutesByName())->map(function ($route) {
            return $route->uri;
        })->toArray(),
        'authUser' => auth()->user(),
        'env' => collect([
            'STRIPE_KEY' => config('services.stripe.key'),
        ]),
    ]);

    return $next($request);
}

?>
```

Then in the header you just add:

```html
<script>
    window.Laravel = JSON.parse('{!! json_encode($laravel) !!}');
</script>
```

## AJAX

* AXIOS (Promise-based HTTP client for JavaScript)
* requests have to include CSRF (Cross-Site Request Forgery)

```javascript
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
```

## Authentication

* **php artisan make:auth** - creates some default authentication routes and views
* *LoginController* uses *AuthenticatesUsers* trait and we needed to override: 
  * the **authenticated** method and set a custom response for when the user is successfully logged in
  * the **sendFailedLoginResponse** method and throw a custom exception
* a new login form is created as a Vue.js modal component which sends an ajax request to the **/login (POST) endpoint**
* when the authentication is successful, the page reloads and the view changes according to the auth check logic inside it 

## Registration

*RegisterController* uses *RegisterUsers* trait and we needed to:
* modify the **create** method, and
* override the **registered** method, where we added the confirm email functionality

The way the confirm email functionality works is that you create a verification token witch is attached to a user as soon as he registered, 
then you send an email witch contains a link with that token as a query parameter and you create a middleware witch will check  the token and remove it thus making the user valid.

## Custom Requests and Controllers

We used custom create and update requests to make controllers as lean as possible.  
*SeriesRequest* has the upload image functionality and it is being shared by both *SeriesCreateRequest* and *SeriesUpdateRequest*.

## Storing uploaded files

* *Illuminate\Http\UploadedFile.php*
* upload images to *storage/app/public/series*:

```php
<?php

/**
 * Upload series image
 *
 * @return $this
 */
public function uploadSeriesImage()
{
    $uploadedImage = $this->image;

    $this->fileName = slugify($this->title) . '.' . $uploadedImage->getClientOriginalExtension();

    $uploadedImage->storePubliclyAs('public/series', $this->fileName);

    return $this;
}

?>
```

* create a symlink from *storage/app/public* to *public/storage* using this artisan command outside of homestead:

```
php artisan storage:link
```

* the images are now available at *domain/storage/series*

## Redis

Redis is a great in memory persistent storage for storing **key:value** pairs. It is able to be persistent, even though it's in the memory, because it has a mechanism of storing a database snapshot in the hard drive storage.
Before using it with laravel you need to include a composer package called **predis/predis**.  
The config is located at **config/database.php** by default.  
A good resource to learn about redis is https://try.redis.io

Redis data types: 
 
* key:value (string)

```php
<?php

Redis::set('key', 'value');
Redis::get('key');

?>
```

* key:value (list)

```php
<?php

Redis::lpush('key', ['value1', 'value2']);
Redis::lrange('key', 0, -1);

?>
```

* key:value (set)

```php
<?php

Redis::sadd('key', ['value1', 'value2']);
Redis::smembers('key');

?>
```

A redis set is a good way to store unique values. This is because a set will not let you add duplicate values like a list does.

To flush all keys stored in the redis database use this command:

```php
<?php

Redis::flushall();

?>
```

## Custom Blade Directives

New custom blade directives are added in the *AppServiceProvider* within the boot method:

```php
<?php

Blade::if('admin', function () {
    return auth()->user()->isAdmin();
});

?>
```

## Laravel Cashier

Laravel Cashier provides an expressive, fluent interface to Stripe's and Braintree's subscription billing services. 
It handles almost all of the boilerplate subscription billing code you are dreading writing. 
In addition to basic subscription management, Cashier can handle coupons, swapping subscription, subscription "quantities", cancellation grace periods, and even generate invoice PDFs.

If you're only performing "one-off" charges and do not offer subscriptions, you should not use Cashier. Instead, use the Stripe and Braintree SDKs directly.

To install Laravel Cashier run this composer command:

```
composer require laravel/cashier
```

Before using Cashier, we'll also need to prepare the database. We need to add several columns to your users table and create a new subscriptions table to hold all of our customer's subscriptions:

```php
<?php

Schema::table('users', function ($table) {
    $table->string('stripe_id')->nullable()->collation('utf8mb4_bin');
    $table->string('card_brand')->nullable();
    $table->string('card_last_four', 4)->nullable();
    $table->timestamp('trial_ends_at')->nullable();
});

Schema::create('subscriptions', function ($table) {
    $table->increments('id');
    $table->unsignedInteger('user_id');
    $table->string('name');
    $table->string('stripe_id')->collation('utf8mb4_bin');
    $table->string('stripe_plan');
    $table->integer('quantity');
    $table->timestamp('trial_ends_at')->nullable();
    $table->timestamp('ends_at')->nullable();
    $table->timestamps();
});

?>
```

Next, add the **Billable** trait to your model definition. This trait provides various methods to allow you to perform common billing tasks, such as creating subscriptions, applying coupons, and updating credit card information:

```php
<?php

use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Billable;
}

?>
```

Finally, you should configure your **Stripe key** in your *services.php* configuration file. You can retrieve your Stripe API keys from the Stripe control panel:

```php
<?php

'stripe' => [
    'model'  => App\User::class,
    'key' => env('STRIPE_KEY'),
    'secret' => env('STRIPE_SECRET'),
],

?>
```

In the stripe dashboard you first need to **create a product** witch in this case is called *laracasts* and is stored in *User::SUBSCRIPTION*.  
Then you **add payment plans** which in this case are *monthly* and *yearly*.  

Now, the way stripe works is that you need to **implement a stripe checkout solution**, like the button with a popup, already prepared.  
When you implement that feature on your website, **after a successful checkout you will get a token representation of the users credit card details**. 

So, with Stripe, sensitive cardholder data does not hit your server, greatly minimizing your PCI compliance burden. Stripe takes care of the hardest parts of PCI compliance, like redacting logs and encrypting card details. Just enable HTTPS on your checkout page, and we'll take over from there. 
Here's the whole workflow:

* The customer arrives at your payment page that includes the Checkout code, loaded over HTTPS.
* The customer clicks the payment button (e.g., Pay with Card), completes the payment form, and clicks Pay $9.99 within the Checkout window (or whatever your Checkout pay button is).
* Checkout sends the payment details directly to Stripe from the customer's browser, assuming the details pass basic validation.
* Stripe returns a token to Checkout, or an error message if the card-network validation fails.
* Checkout takes the returned token and stores it in the page's primary form in a hidden element named stripeToken.
* Checkout submits the form to your server.
* Your server uses the posted token to charge the card.

This is our custom checkout form implementation:

```vue
<template>
    <div>
        <button class="btn btn-success" @click="subscribe('monthly')">Subscribe to $9.99 Monthly</button>
        <button class="btn btn-info" @click="subscribe('yearly')">Subscribe to $99.99 Yearly</button>
    </div>
</template>

<script>
    import Swal from 'sweetalert';
    import Axios from 'axios';

    export default {
        props: ['dataUser'],
        mounted() {
            let vm = this;

            this.handler = StripeCheckout.configure({
                key: window.Laravel.env.STRIPE_KEY,
                image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                locale: 'auto',
                token(token) {
                    // You can access the token ID with `token.id`.
                    // Get the token ID to your server-side code for use.
                    Swal({text: 'Please wait while we subscribe you to a plan ...', buttons: false});
                    Axios.post('/subscribe', {
                        stripeToken: token.id,
                        plan: vm.plan,
                    }).then(resp => {
                        Swal({text: 'Successfully subscribed', icon: 'success'})
                            .then(() => {
                                window.location = route('profile', vm.user.id);
                            });
                    }).catch(error => {
                        console.log(error);
                    });
                }
            });
        },
        data() {
            return {
                user: JSON.parse(this.dataUser),
                plan: '',
                amount: 0,
                handler: null,
            };
        },
        methods: {
            subscribe(plan) {
                this.plan = (plan === 'monthly') ? 'monthly' : 'yearly';
                this.amount = (plan === 'monthly') ? 999 : 9999;

                this.handler.open({
                    name: 'Laracasts',
                    description: 'Laracasts Subscription',
                    amount: this.amount,
                    email: this.user.email,
                });
            },
        },
    }
</script>
```

This is how our server charges the card:

```php
<?php

/**
 * Subscribe
 *
 * @param Request $request
 * @return \Laravel\Cashier\Subscription
 */
public function subscribe(Request $request)
{
    return auth()->user()->newSubscription(User::SUBSCRIPTION, $request->plan)
                 ->create($request->stripeToken);
}

?>
```

## Laravel DebugBar

Laravel Debugbar is a great tool that gives you information about the current route, controller, views, queries and etc. It loads with each page load and it is located at the bottom of the browser window.  

To install it run this composer command:

```
composer require barryvdh/laravel-debugbar --dev
```

## Laravel Horizon

Horizon provides a beautiful dashboard and code-driven configuration for your Laravel powered Redis queues. Horizon allows you to easily monitor key metrics of your queue system such as job throughput, runtime, and job failures.

You may use Composer to install Horizon into your Laravel project:

```
composer require laravel/horizon
```

After installing Horizon, publish its assets using the *horizon:install* Artisan command:

```
php artisan horizon:install
```

You should also create the *failed_jobs* table which Laravel will use to store any failed queue jobs:

```
php artisan queue:failed-table

php artisan migrate
```

Once you have configured your workers in the *config/horizon.php* configuration file, you may start Horizon using the *horizon* Artisan command. This single command will start all of your configured workers:

```
php artisan horizon
```

You may pause the Horizon process and instruct it to continue processing jobs using the *horizon:pause* and *horizon:continue* Artisan commands:

```
php artisan horizon:pause

php artisan horizon:continue
```

You may gracefully terminate the master Horizon process on your machine using the *horizon:terminate* Artisan command. Any jobs that Horizon is currently processing will be completed and then Horizon will exit:

```
php artisan horizon:terminate
```

Dashboard is located at **{domain}/horizon**

## Latavel Telescope

Laravel Telescope is an elegant debug assistant for the Laravel framework. Telescope provides insight into the requests coming into your application, exceptions, log entries, database queries, queued jobs, mail, notifications, cache operations, scheduled tasks, variable dumps and more. Telescope makes a wonderful companion to your local Laravel development environment.

You may use Composer to install Telescope into your Laravel project:

```
composer require laravel/telescope
```

After installing Telescope, publish its assets using the *telescope:install* Artisan command. After installing Telescope, you should also run the *migrate* command:

```
php artisan telescope:install

php artisan migrate
```

Telescope exposes a dashboard at **{domain}/telescope**. By default, you will only be able to access this dashboard in the local environment.


