<?php

use Faker\Generator as Faker;
use Laracasts\User;

$factory->define(Laracasts\Series::class, function (Faker $faker) {
    $title = $faker->sentence(5);

    return [
        'user_id' => factory(User::class)->create()->id,
        'title' => $title,
        'description' => $faker->paragraph(),
        'image_url' => '/series/350x234.png', // $faker->imageUrl()
        'featured' => false,
    ];
});
