<?php

use Faker\Generator as Faker;
use Laracasts\Series;

$factory->define(Laracasts\Lesson::class, function (Faker $faker) {
    return [
        'series_id' => function () {
            return factory(Series::class)->create()->id;
        },
        'title' => $faker->sentence(3),
        'description' => $faker->paragraph(3),
        'episode_number' => 100,
        'video_id' => '259411563',
        'premium' => true,
    ];
});
