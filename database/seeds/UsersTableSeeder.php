<?php

use Illuminate\Database\Seeder;
use Laracasts\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Ivan Stantic',
            'email' => 'stantic.ivan@gmail.com',
        ]);

        factory(User::class)->create([
            'name' => 'Stantic Ivan',
            'email' => 'ivan88.vts@gmail.com',
        ]);
    }
}
