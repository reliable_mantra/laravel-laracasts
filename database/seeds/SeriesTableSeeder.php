<?php

use Illuminate\Database\Seeder;
use Laracasts\Series;
use Laracasts\User;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Series::create([
            'user_id' => User::where('email', '=', 'stantic.ivan@gmail.com')->first()->id,
            'title' => 'Laravel TDD Essentials',
            'description' => 'Test Driven Development in Laravel.',
            'image_url' => '/series/laravel.jpg',
            'featured' => true,
        ]);

        Series::create([
            'user_id' => User::where('email', '=', 'ivan88.vts@gmail.com')->first()->id,
            'title' => 'Vue.js Essentials',
            'description' => 'The best Vue.js tutorial out there.',
            'image_url' => '/series/best-vue-js-tutorial.jpg',
            'featured' => true,
        ]);

        factory(Series::class, 2)->create();
        factory(Series::class)->create(['user_id' => 3]);
    }
}
