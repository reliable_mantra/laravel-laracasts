<?php

use Illuminate\Database\Seeder;
use Laracasts\Lesson;
use Laracasts\Series;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $series1 = Series::where('title', '=', 'Laravel TDD Essentials')->first();
        $series2 = Series::where('title', '=', 'Vue.js Essentials')->first();

        // Laravel TDD Essentials
        Lesson::create([
            'title' => 'Test Driven Laravel from Scratch',
            'description' => 'Why you should start using Test Driven Development',
            'series_id' => $series1->id,
            'episode_number' => 10,
            'video_id' => '151390908',
            'premium' => false,
        ]);

        Lesson::create([
            'title' => 'Disabling exception handling in acceptance tests',
            'description' => 'Skip exception and see where errors are coming from',
            'series_id' => $series1->id,
            'episode_number' => 20,
            'video_id' => '152568424',
            'premium' => false,
        ]);

        Lesson::create([
            'title' => 'Writing Your Own Test Doubles',
            'description' => 'How to write your own test doubles',
            'series_id' => $series1->id,
            'episode_number' => 30,
            'video_id' => '152727171',
            'premium' => true,
        ]);

        Lesson::create([
            'title' => 'Preventing API Drift with Contract Tests',
            'description' => 'Testing APIs with Contract Tests',
            'series_id' => $series1->id,
            'episode_number' => 40,
            'video_id' => '153575893',
            'premium' => true,
        ]);

        Lesson::create([
            'title' => 'Three Approaches to Testing Events in Laravel',
            'description' => 'Learn three different approaches to testing in Laravel',
            'series_id' => $series1->id,
            'episode_number' => 50,
            'video_id' => '178931955',
            'premium' => true,
        ]);

        // Vue.js Essentials
        Lesson::create([
            'title' => 'Introduction to Vue.js',
            'description' => 'What is Vue.js and why you should use it',
            'series_id' => $series2->id,
            'episode_number' => 10,
            'video_id' => '200384768',
            'premium' => false,
        ]);

        Lesson::create([
            'title' => 'Local environment',
            'description' => 'Setting up a local development environment',
            'series_id' => $series2->id,
            'episode_number' => 20,
            'video_id' => '200421652',
            'premium' => false,
        ]);

        Lesson::create([
            'title' => 'Hello World',
            'description' => 'Vue instances & string interpolation',
            'series_id' => $series2->id,
            'episode_number' => 30,
            'video_id' => '200417007',
            'premium' => true,
        ]);

        Lesson::create([
            'title' => 'Vue.js Beginner\'s Course - Set Up Our First App',
            'description' => 'Set Up Our First App',
            'series_id' => $series2->id,
            'episode_number' => 40,
            'video_id' => '239317293',
            'premium' => true,
        ]);

        Lesson::create([
            'title' => 'Vue.js Beginner’s Course - Rendering Data On The Page',
            'description' => 'Rendering Data On The Page',
            'series_id' => $series2->id,
            'episode_number' => 50,
            'video_id' => '242534261',
            'premium' => true,
        ]);

        factory(Lesson::class, 5)->create(['series_id' => 3]);
        factory(Lesson::class, 5)->create(['series_id' => 4]);
        factory(Lesson::class, 5)->create(['series_id' => 5]);
    }
}
