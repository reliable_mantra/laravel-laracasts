<?php

namespace Tests\Unit;

use Laracasts\Lesson;
use Laracasts\Series;
use Laracasts\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LessonTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated_user_can_get_next_and_previous_lessons_from_a_lesson()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $series = factory(Series::class)->create();
        $lesson1 = factory(Lesson::class)->create([
            'episode_number' => 200,
            'series_id' => $series->id,
        ]);
        $lesson2 = factory(Lesson::class)->create([
            'episode_number' => 100,
            'series_id' => $series->id,
        ]);
        $lesson3 = factory(Lesson::class)->create([
            'episode_number' => 300,
            'series_id' => $series->id,
        ]);

        $this->assertEquals($lesson1->getNextLesson()->id, $lesson3->id);
        $this->assertEquals($lesson3->getPrevLesson()->id, $lesson1->id);
        $this->assertEquals($lesson2->getNextLesson()->id, $lesson1->id);
        $this->assertEquals($lesson2->getPrevLesson()->id, $lesson2->id);
        $this->assertEquals($lesson3->getNextLesson()->id, $lesson3->id);
    }
}
