<?php

namespace Tests\Unit;

use Illuminate\Support\Collection;
use Laracasts\Lesson;
use Laracasts\Series;
use Laracasts\User;
use Redis;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_complete_a_lesson()
    {
        $this->flushRedis();

        $user = factory(User::class)->create();
        $series = factory(Series::class)->create();
        $lesson = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series->id]);

        $user->completeLesson($lesson);
        $user->completeLesson($lesson2);

        $this->assertEquals(Redis::smembers('user:1:series:1'), [1, 2]);

        $this->assertEquals($user->getNumberOfCompletedLessonsForASeries($series), 2);
    }

    public function test_can_get_percentage_completed_for_series_for_a_user()
    {
        $this->flushRedis();

        $user = factory(User::class)->create();
        $series = factory(Series::class)->create();
        $lesson = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series->id]);
        factory(Lesson::class)->create(['series_id' => $series->id]);
        factory(Lesson::class)->create(['series_id' => $series->id]);

        $user->completeLesson($lesson);
        $user->completeLesson($lesson2);

        $this->assertEquals($user->percentageCompletedForSeries($series), 50);
    }

    public function test_can_know_if_a_user_has_started_a_series()
    {
        $this->flushRedis();

        $user = factory(User::class)->create();
        $series = factory(Series::class)->create();
        $lesson = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson3 = factory(Lesson::class)->create();

        $user->completeLesson($lesson2);

        $this->assertTrue($user->hasStartedSeries($lesson->series));
        $this->assertFalse($user->hasStartedSeries($lesson3->series));
    }

    public function test_can_get_completed_lessons_for_a_series()
    {
        $this->flushRedis();

        /** @var \Laracasts\User $user */
        $user    = factory(User::class)->create();
        $series  = factory(Series::class)->create();
        $lesson  = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson3 = factory(Lesson::class)->create(['series_id' => $series->id]);

        $user->completeLesson($lesson);
        $user->completeLesson($lesson2);

        $completedLessons = $user->getCompletedLessons($lesson->series);

        $this->assertInstanceOf(Collection::class, $completedLessons);
        $this->assertInstanceOf(Lesson::class, $completedLessons->random());

        $completedLessonsIds = $completedLessons->pluck('id')->all();

        $this->assertTrue(in_array($lesson->id, $completedLessonsIds));
        $this->assertTrue(in_array($lesson2->id, $completedLessonsIds));
        $this->assertFalse(in_array($lesson3->id, $completedLessonsIds));
    }

    public function test_can_check_if_user_has_completed_lesson()
    {
        $this->flushRedis();

        $user = factory(User::class)->create();
        $this->actingAs($user);

        $series = factory(Series::class)->create();
        $lesson1 = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series->id]);

        $user->completeLesson($lesson1);

        $this->assertTrue($user->hasCompletedLesson($lesson1));
        $this->assertFalse($user->hasCompletedLesson($lesson2));
    }

    public function test_can_get_all_series_being_watched_by_user()
    {
        $this->flushRedis();

        /** @var \Laracasts\User $user */
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $series1 = factory(Series::class)->create();
        $lesson1 = factory(Lesson::class)->create(['series_id' => $series1->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series1->id]);
        $series2 = factory(Series::class)->create();
        $lesson3 = factory(Lesson::class)->create(['series_id' => $series2->id]);
        $lesson4 = factory(Lesson::class)->create(['series_id' => $series2->id]);
        $series3 = factory(Series::class)->create();
        $lesson5 = factory(Lesson::class)->create(['series_id' => $series3->id]);
        $lesson6 = factory(Lesson::class)->create(['series_id' => $series3->id]);

        $user->completeLesson($lesson1);
        $user->completeLesson($lesson3);

        $startedSeries = $user->seriesBeingWatched();

        $this->assertInstanceOf(Collection::class, $startedSeries);
        $this->assertInstanceOf(Series::class, $startedSeries->random());

        $idsOfStartedSeries = $startedSeries->pluck('id')->all();
        $this->assertTrue(in_array($lesson1->series->id, $idsOfStartedSeries));
        $this->assertTrue(in_array($lesson3->series->id, $idsOfStartedSeries));
        $this->assertFalse(in_array($lesson6->series->id, $idsOfStartedSeries));
    }

    public function test_can_get_number_of_completed_lessons_for_a_user()
    {
        $this->flushRedis();

        /** @var \Laracasts\User $user */
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $series1 = factory(Series::class)->create();
        $lesson1 = factory(Lesson::class)->create(['series_id' => $series1->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series1->id]);
        $series2 = factory(Series::class)->create();
        $lesson3 = factory(Lesson::class)->create(['series_id' => $series2->id]);
        $lesson4 = factory(Lesson::class)->create(['series_id' => $series2->id]);
        $lesson5 = factory(Lesson::class)->create(['series_id' => $series2->id]);

        $user->completeLesson($lesson1);
        $user->completeLesson($lesson3);
        $user->completeLesson($lesson5);

        $this->assertEquals(3, $user->getTotalNumberOfCompletedLessons());
    }

    public function test_can_get_next_lesson_to_be_watched_by_user()
    {
        $this->flushRedis();

        /** @var \Laracasts\User $user */
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $series1 = factory(Series::class)->create();
        $lesson1 = factory(Lesson::class)->create([
            'series_id' => $series1->id,
            'episode_number' => 100,
        ]);
        $lesson2 = factory(Lesson::class)->create([
            'series_id' => $series1->id,
            'episode_number' => 200,
        ]);
        $lesson3 = factory(Lesson::class)->create([
            'series_id' => $series1->id,
            'episode_number' => 300,
        ]);
        $lesson4 = factory(Lesson::class)->create([
            'series_id' => $series1->id,
            'episode_number' => 400,
        ]);

        $user->completeLesson($lesson1);
        $user->completeLesson($lesson2);

        $nextLesson = $user->getNextLessonToWatch($lesson1->series);

        $this->assertEquals($lesson3->id, $nextLesson->id);

        $user->completeLesson($lesson3);

        $this->assertEquals($lesson4->id, $user->getNextLessonToWatch($lesson1->series)->id);
    }
}
