<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Mail;
use Laracasts\Mail\ConfirmYourEmail;
use Laracasts\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_has_a_token_after_registration()
    {
        Mail::fake();

        $this->post('/register', [
            'name' => 'Ivan Stantic',
            'email' => 'stantic.ivan@gmail.com',
            'password' => 'secret',
        ])->assertRedirect();

        $user = User::find(1);
        $this->assertNotNull($user->confirm_token);
        $this->assertFalse($user->isConfirmed());
    }

    public function test_an_email_is_sent_to_newly_registered_users()
    {
        Mail::fake();

        $this->post('/register', [
            'name' => 'Ivan Stantic',
            'email' => 'stantic.ivan@gmail.com',
            'password' => 'secret',
        ])->assertRedirect();

        Mail::assertQueued(ConfirmYourEmail::class);
    }
}
