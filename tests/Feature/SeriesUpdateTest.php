<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laracasts\Series;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SeriesUpdateTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_update_a_series()
    {
        $this->loginAdmin();

        Storage::fake(config('filesystems.default'));

        $series = factory(Series::class)->create();

        $data = [
            'title' => 'new series title',
            'description' => 'new series description',
            'image' => UploadedFile::fake()->image('image-series.png'),
        ];

        $this->put(route('series.update', $series->slug), $data)
             ->assertRedirect(route('series.index'))
             ->assertSessionHas('success', 'Successfully updated series');

        Storage::disk(config('filesystems.default'))->assertExists(
            'public/series/' . slugify($data['title']) . '.png'
        );

        $this->assertDatabaseHas('series', [
            'title' => $data['title'],
            'image_url' => 'series/' . slugify($data['title']) . '.png',
        ]);
    }

    public function test_an_image_is_not_required_to_update_a_series()
    {
        $this->loginAdmin();

        Storage::fake(config('filesystems.default'));

        $series = factory(Series::class)->create();

        $data = [
            'title' => 'new series title',
            'description' => 'new series description',
        ];

        $this->put(route('series.update', $series->slug), $data)
             ->assertRedirect(route('series.index'))
             ->assertSessionHas('success', 'Successfully updated series');

        Storage::disk(config('filesystems.default'))->assertMissing(
            'public/series/' . slugify($data['title']) . '.png'
        );

        $this->assertDatabaseHas('series', [
            'title' => $data['title'],
            'image_url' => $series->image_url,
        ]);
    }
}
