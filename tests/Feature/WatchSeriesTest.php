<?php

namespace Tests\Feature;

use Laracasts\Lesson;
use Laracasts\Series;
use Laracasts\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WatchSeriesTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_complete_a_series()
    {
        $this->withoutExceptionHandling();
        $this->flushRedis();

        $user = factory(User::class)->create();
        $this->actingAs($user);

        $series = factory(Series::class)->create();
        $lesson1 = factory(Lesson::class)->create(['series_id' => $series->id]);
        $lesson2 = factory(Lesson::class)->create(['series_id' => $series->id]);

        $response = $this->post("/series/complete-lesson/{$lesson1->id}", []);

        $response->assertStatus(200);
        $response->assertJson(['status' => 'ok']);
        $this->assertTrue($user->hasCompletedLesson($lesson1));
        $this->assertFalse($user->hasCompletedLesson($lesson2));
    }
}
