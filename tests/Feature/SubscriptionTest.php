<?php

namespace Tests\Feature;

use Laracasts\Lesson;
use Laracasts\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubscriptionTest extends TestCase
{
    use RefreshDatabase;

    public function fakeSubscribe(User $user)
    {
        $user->subscriptions()->create([
            'name' => 'laracasts',
            'stripe_id' => 'FAKE_STRIPE_ID',
            'stripe_plan' => 'yearly',
            'quantity' => 1,
        ]);
    }

    public function test_a_user_without_a_plan_cannot_watch_premium_lessons()
    {
        $user = factory(User::class)->create();
        $lesson1 = factory(Lesson::class)->create(['premium' => 1]);
        $lesson2 = factory(Lesson::class)->create(['premium' => 0]);

        $this->actingAs($user);

        $this->get("/series/{$lesson1->series->slug}/lesson/{$lesson1->id}")
             ->assertRedirect('/subscribe');
        $this->get("/series/{$lesson2->series->slug}/lesson/{$lesson2->id}")
             ->assertViewIs('templates.watch');
    }

    public function test_a_user_on_any_plan_can_watch_all_lessons()
    {
        $user = factory(User::class)->create();
        $lesson1 = factory(Lesson::class)->create(['premium' => 1]);
        $lesson2 = factory(Lesson::class)->create(['premium' => 0]);

        $this->actingAs($user);
        $this->fakeSubscribe($user);

        $this->get("/series/{$lesson1->series->slug}/lesson/{$lesson1->id}")
             ->assertViewIs('templates.watch');
        $this->get("/series/{$lesson2->series->slug}/lesson/{$lesson2->id}")
             ->assertViewIs('templates.watch');
    }
}
