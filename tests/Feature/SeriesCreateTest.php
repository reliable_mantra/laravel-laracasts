<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laracasts\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SeriesCreateTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_create_a_series()
    {
        $this->loginAdmin();

        Storage::fake(config('filesystems.default'));

        $data = [
            'title' => 'vuejs for the best',
            'description' => 'the best vue casts ever',
            'image' => UploadedFile::fake()->image('image-series.png')
        ];

        $this->post('/admin/series', $data)
             ->assertRedirect()
             ->assertSessionHas('success', 'Series created successfully');

        Storage::disk(config('filesystems.default'))->assertExists(
            'public/series/' . slugify($data['title']) . '.png'
        );

        $this->assertDatabaseHas('series', [
            'title' => $data['title'],
        ]);
    }

    public function test_a_series_must_be_created_with_a_title()
    {
        $this->loginAdmin();

        $this->post('/admin/series', [
            'description' => 'the best vue casts ever',
            'image' => UploadedFile::fake()->image('image-series.png'),
        ])->assertSessionHasErrors('title');
    }

    public function test_a_series_must_be_created_with_a_description()
    {
        $this->loginAdmin();

        $this->post('/admin/series', [
            'title' => 'vuejs for the best',
            'image' => UploadedFile::fake()->image('image-series.png'),
        ])->assertSessionHasErrors('description');
    }

    public function test_a_series_must_be_created_with_a_image()
    {
        $this->loginAdmin();

        $this->post('/admin/series', [
            'title' => 'vuejs for the best',
            'description' => 'the best vue casts ever',
        ])->assertSessionHasErrors('image');
    }

    public function test_a_series_must_be_created_with_a_image_which_is_actually_a_image()
    {
        $this->loginAdmin();

        $this->post('/admin/series', [
            'title' => 'vuejs for the best',
            'description' => 'the best vue casts ever',
            'image' => 'STRING_INVALID_IMAGE',
        ])->assertSessionHasErrors('image');
    }

    public function test_only_administrators_can_create_series()
    {
        $this->actingAs(factory(User::class)->create());
        $this->post('admin/series')->assertSessionHas('error', 'You are not authorized to perform this action.');
    }
}
