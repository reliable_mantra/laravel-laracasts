<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Config;
use Laracasts\User;
use Redis;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Create and log in an admin user
     */
    public function loginAdmin()
    {
        $user = factory(User::class)->create();

        Config::push('laracasts.administrators', $user->email);

        $this->actingAs($user);
    }

    /**
     * Delete all keys from Redis
     */
    public function flushRedis()
    {
        Redis::flushall();
    }
}
