<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'FrontendController@welcome')->name('home');
Route::get('/profile/{user}', 'ProfilesController@index')->name('profile');
Route::get('/series', 'FrontendController@showAllseries')->name('series-all');
Route::get('/series/{series}', 'FrontendController@series')->name('series');
Route::get('/series/{series}/lesson/{lesson}', 'WatchSeriesController@showLesson')->name('series.watch.lesson');
Route::get('/register/confirm', 'ConfirmEmailController@index')->name('confirm-email');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware('auth')->group(function () {
    Route::post('/profile/{user}', 'ProfilesController@update')->name('profile.update');
    Route::post('/profile/card/update', 'ProfilesController@cardUpdate')->name('profile.card.update');
    Route::get('/subscribe', 'SubscriptionsController@showSubscriptionForm')->name('subscription.form');
    Route::post('/subscribe', 'SubscriptionsController@subscribe')->name('subscribe');
    Route::post('/subscription/change', 'SubscriptionsController@change')->name('subscription.change');
    Route::get('/watch-series/{series}', 'WatchSeriesController@index')->name('series.watch');
    Route::post(
        '/series/complete-lesson/{lesson}',
        'WatchSeriesController@completeLesson'
    )->name('series.lesson.complete');
});
