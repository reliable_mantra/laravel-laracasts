module.exports = function(error) {
    if (error.response.status === 422) {
        window.noty({
            message: 'You have validation errors. Please try again.',
            type: 'danger',
        });
    }

    window.noty({
        message: 'Something went wrong. Please refresh the page.',
    });

    console.log(error);
};
