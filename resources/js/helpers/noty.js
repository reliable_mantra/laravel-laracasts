window.events = new Vue();

module.exports = function(notification) {
    window.events.$emit('notification', notification);
};
