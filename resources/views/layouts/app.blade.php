<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('assets/css/core.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/thesaas.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('assets/img/apple-touch-icon.png') }}">
    <link rel="icon" href="{{ asset('assets/img/favicon.png') }}">

    <script>
        window.Laravel = JSON.parse('{!! json_encode($laravel) !!}');
    </script>

    @yield('scripts')
</head>

<body>

<!-- App -->
<div id="app">


    <!-- Topbar -->
    @yield('topbar')
    <!-- END Topbar -->


    <!-- Header -->
    @yield('header')
    <!-- END Header -->


    <!-- Main container -->
    <main class="main-content">
        @yield('content')
    </main>
    <!-- END Main container -->


    <vue-noty></vue-noty>
    @guest
        <vue-login></vue-login>
    @endguest


    <!-- Footer -->
    <footer class="site-footer">
        <div class="container">
            <div class="row gap-y justify-content-center">
                <div class="col-12 col-lg-6">
                    <ul class="nav nav-primary nav-hero">
                        <li class="nav-item hidden-sm-down">
                            <a class="nav-link" href="/">Laracasts</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- END Footer -->


</div>
<!-- END App -->

<!-- Scripts -->
<script src="{{ asset('assets/js/core.min.js') }}"></script>
<script src="{{ asset('assets/js/thesaas.min.js') }}"></script>
<script src="{{ asset('assets/js/script.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
