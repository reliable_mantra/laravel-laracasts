@php
    $isInverse = $inverse ?? false;
@endphp

<nav class="topbar topbar-expand-md topbar-sticky @if($isInverse) topbar-inverse @endif">
    <div class="container">

        <div class="topbar-left">
            <button class="topbar-toggler">&#9776;</button>
            <a class="topbar-brand" href="/">
                LARACASTS
            </a>
        </div>


        <div class="topbar-right">
            <ul class="topbar-nav nav nav-navbar">
                @auth
                    @admin
                        <li class="nav-item"><a href="{{ route('series.index') }}" class="nav-link">All Series</a></li>
                        <li class="nav-item"><a href="{{ route('series.create') }}" class="nav-link">Create Series</a></li>
                    @endadmin

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('profile', auth()->user()->id) }}">
                        Hey {{ auth()->user()->name }}
                        </a>
                        <nav class="nav-submenu">
                            @if(!auth()->user()->subscribed())
                                <a href="{{ route('subscribe') }}" class="nav-link">Subscribe</a>
                            @endif
                            <a href="{{ route('logout') }}" class="nav-link">Logout</a>
                        </nav>
                    </li>
                @endauth

                @guest
                    <li class="nav-item"><a href="{{ route('series-all') }}" class="nav-link">All Series</a></li>

                    @if(Request::route()->getName() !== 'login')
                        <li class="nav-item"><a class="nav-link" href="javascript:;" data-toggle="modal" data-target="#loginModal">Login</a></li>
                    @endif
                @endguest
            </ul>
        </div>

    </div>
</nav>