<div class="card mb-30">
    <div class="row">
        <div class="col-12 col-md-4 align-self-center">
            <a href=""><img src="{{ $series->image_path }}" alt="..."></a>
        </div>

        <div class="col-12 col-md-8">
            <div class="card-block">
                <h4 class="card-title">{{ $series->title }}</h4>
                <p class="card-subtitle"><small>Author: <a href="{{ route('profile', $series->user_id) }}">{{ $series->user->name }}</a></small></p>

                <p class="card-text">{{ $series->description }}</p>
                <a class="fw-600 fs-12" href="{{ route('series', $series->slug) }}">Read more <i class="fa fa-chevron-right fs-9 pl-8"></i></a>
            </div>
        </div>
    </div>
</div>