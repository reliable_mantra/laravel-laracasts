@extends('layouts.app')

@section('topbar')
    @include('template-parts.topbar')
@endsection

@section('content')
    <div class="mh-fullscreen bg-img center-vh p-20" style="background-image: url(assets/img/bg-girl.jpg);">

        <div class="card card-shadowed p-50 w-400 mb-0" style="max-width: 100%">
            <h5 class="text-uppercase text-center">Login</h5>
            <br><br>

            <form class="form-type-material" action="/login" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email address" name="email">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                </div>

                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description" for="remember">{{ __('Remember Me') }}</span>
                    </label>
                </div>

                <br>
                <button class="btn btn-bold btn-block btn-primary" type="submit">Login</button>
            </form>

            <hr class="w-30">

            <p class="text-center text-muted fs-13 mt-20">No account yet? <a href="/register">Sign Up</a></p>
        </div>

    </div>
@endsection
