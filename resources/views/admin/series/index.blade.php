@extends('layouts.app')

@section('topbar')
    @include('template-parts.topbar', ['inverse' => true])
@endsection

@section('header')
    <header class="header header-inverse">
        <div class="container text-center">

            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">

                    <h1>All series</h1>
                    <p class="fs-20 opacity-70">Let's make the world a better place for coders</p>

                </div>
            </div>

        </div>
    </header>
@stop

@section('content')
    <div class="section bg-grey">
        <div class="container bg-white b-1 border-secondary">

            <div class="row gap-y">
                <div class="col-12">

                    <vue-series init_series="{{ $series }}"></vue-series>

                </div>
            </div>
        </div>
    </div>
@stop
