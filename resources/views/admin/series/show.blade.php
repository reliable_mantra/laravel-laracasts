@extends('layouts.app')

@section('topbar')
    @include('template-parts.topbar', ['inverse' => true])
@endsection

@section('header')
    <header class="header header-inverse">
        <div class="container text-center">

            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">

                    <h1>{{ $series->title }}</h1>
                    <p class="fs-20 opacity-70">Customize your series lessons</p>

                </div>
            </div>

        </div>
    </header>
@stop

@section('content')
    <div class="section bg-gray">
        <div class="container">

            <div class="row gap-y">
                <div class="col-12">

                    <vue-lessons data-series="{{ $series }}" data-lessons="{{ $series->lessons }}"></vue-lessons>

                </div>
            </div>
        </div>
    </div>
@stop