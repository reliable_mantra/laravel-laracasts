@extends('layouts.app')

@section('topbar')
    @include('template-parts.topbar', ['inverse' => true])
@endsection

@section('header')
    <header class="header header-inverse">
        <div class="container text-center">

            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">

                    <h1>{{ $user->name }}</h1>
                    <p class="fs-20 opacity-70">{{ $user->email }}</p>
                    <br>
                    <h1>{{ $user->getTotalNumberOfCompletedLessons() }}</h1>
                    <p class="fs-20 opacity-70">Lessons completed</p>
                </div>
            </div>

        </div>
    </header>
@stop

@section('content')
    <section class="section" id="section-vtab">
        <div class="container">
            <header class="section-header">
                <h2>Series being watched ...</h2>
                <hr>
            </header>

            @forelse($series as $s)
                @include('template-parts.series', ['series' => $s])
            @empty
            @endforelse

        </div>
    </section>

    @if(auth()->id() === $user->id)
        @php
            $subscription = auth()->user()->subscriptions->first();
        @endphp
        <section class="section bg-gray" id="section-vtab">
            <div class="container">
                @if (Session::has('success'))
                    <vue-noty data-notification="{{ Session::get('success') }}" data-type="success"></vue-noty>
                @endif

                <header class="section-header">
                    <h2>Edit your profile</h2>
                    <hr>
                </header>


                <div class="row gap-5">


                    <div class="col-12 col-md-4">
                        <ul class="nav nav-vertical">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#details">
                                    <h6>Personal details</h6>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#subscription">
                                    <h6>Subscription</h6>
                                </a>
                            </li>
                            @if(auth()->user()->card_brand)
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#card-details">
                                        <h6>Card details</h6>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>


                    <div class="col-12 col-md-8">
                        <div class="tab-content">

                            <div class="tab-pane fade show active" id="details">
                                <p>Name: <strong>{{ $user->name }}</strong></p>
                                <p>Email: <strong>{{ $user->email }}</strong></p>

                                <h5>Update</h5>
                                <form action="{{ route('profile.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <input class="form-control form-control-lg" type="text" name="name" placeholder="{{ $user->name ?? 'Your name' }}">
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control form-control-lg" type="text" name="email" placeholder="{{ $user->email ?? 'Your email' }}">
                                    </div>

                                    <button class="btn btn-lg btn-primary btn-block" type="submit">Save changes</button>
                                </form>
                            </div>

                            <div class="tab-pane fade" id="subscription">
                                <form action="{{ route('subscription.change') }}" method="post">
                                    {{ csrf_field() }}
                                    <h3>
                                        Your current plan:
                                        @if($subscription)
                                            <span class="badge badge-primary"><strong>{{ $subscription->stripe_plan }}</strong></span>
                                        @else
                                            <span class="badge badge-danger">NO PLAN</span>
                                        @endif
                                    </h3>
                                    <br>
                                    @if($subscription)
                                        <select name="plan" class="form-control">
                                            <option value="monthly">Monthly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                        <br>
                                        <p>
                                            <button class="btn btn-primary" type="submit">Change plan</button>
                                        </p>
                                    @endif

                                </form>
                            </div>

                            @if(auth()->user()->card_brand)
                                <div class="tab-pane fade" id="card-details">
                                    <div class="row">
                                        <h3 style="display: inline-block;">
                                            Your current card:
                                            <span class="badge badge-sm badge-primary">
                                                <strong>{{ auth()->user()->card_brand }}:{{ auth()->user()->card_last_four }}</strong>
                                            </span>
                                        </h3>
                                    </div>
                                    <div class="row" style="margin-top: 30px;">
                                        <vue-update-card data-user="{{ auth()->user() }}"></vue-update-card>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>


                </div>


            </div>
        </section>
    @endif

@endsection

@section('scripts')
    <script src="https://checkout.stripe.com/checkout.js"></script>
@endsection
