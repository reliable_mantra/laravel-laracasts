@extends('layouts.app')

@section('topbar')
    @include('template-parts.topbar', ['inverse' => true])
@endsection

@section('header')
    <header class="header header-inverse">
        <div class="container text-center">

            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">

                    <h1>What do you wanna learn next ? !</h1>
                </div>
            </div>

        </div>
    </header>
@stop

@section('content')
    <section class="section" id="section-vtab">
        <div class="container">
            <header class="section-header">
                <h2>Here's all the fun stuff !</h2>
            </header>

            @forelse($series as $s)
                @include('template-parts.series', ['series' => $s])
            @empty
            @endforelse

        </div>
    </section>
    <section class="section bg-gray" id="section-vtab">
        <div class="container">
        </div>
    </section>

@endsection
